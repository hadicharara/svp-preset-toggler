import xbmc
import urllib2
import socket
import xbmcaddon

if __name__ == '__main__':
    my_addon = xbmcaddon.Addon()
    serverIP = my_addon.getSetting('ip')

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((serverIP,9901))
    if result != 0:
        xbmc.executebuiltin('Notification(Cannot connect to SVP http server!,Check if the server is online and try again.,5000)')
        sys.exit()
    sock.close()

    currentProfileID = urllib2.urlopen("http://"+serverIP+":9901?profiles."+my_addon.getSetting('url.sub')+".fi_preset").read()[59:]

    if currentProfileID == "0":
        xbmc.executebuiltin('Notification(Current SVP profile:,Film,5000)')
    
    elif currentProfileID == "1":
        xbmc.executebuiltin('Notification(Current SVP profile:,Animation,5000)')
    
    else:
        xbmc.executebuiltin('Notification(Current SVP profile:,Unknown,5000)')