import xbmc
import xbmcgui
import xbmcaddon
import urllib2
import socket
import re

# create a class for your addon, we need this to get info about your addon
my_addon = xbmcaddon.Addon()

# this is the entry point of your addon, execution of your script will start here
if (__name__ == '__main__'):
    serverIP = my_addon.getSetting('ip')

    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = sock.connect_ex((serverIP,9901))
    if result != 0:
        xbmc.executebuiltin('Notification(Cannot connect to SVP http server!,Check if the server is online and try again.,5000)')
        sys.exit()
    sock.close()

    currentProfileID = urllib2.urlopen("http://"+serverIP+":9901?profiles."+my_addon.getSetting('url.sub')+".fi_preset").read()[59:]

    if currentProfileID == "0":
        nextID = "1"
    
    elif currentProfileID == "1":
        nextID = "0"
    
    currentProfileID = urllib2.urlopen("http://"+serverIP+":9901?profiles."+my_addon.getSetting('url.sub')+".fi_preset="+nextID).read()[59:]
    
    if nextID == "0":
        xbmc.executebuiltin('Notification(Current SVP profile:,Film,5000)')
    
    elif nextID == "1":
        xbmc.executebuiltin('Notification(Current SVP profile:,Animation,5000)')
    
    else:
        xbmc.executebuiltin('Notification(Current SVP profile:,Unknown,5000)')
    sys.exit()